import 'package:flutter/material.dart';
import 'package:rating_app/pictures/picture_view.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
        scaffoldBackgroundColor: Colors.grey[400],
        textTheme: const TextTheme().apply(
          bodyColor: Colors.black,
          displayColor: Colors.black,
        ),
        snackBarTheme: const SnackBarThemeData(
          shape: StadiumBorder(),
          contentTextStyle: TextStyle(fontSize: 18.0),
        ),
      ),
      home: FirstPage(),
    );
  }
}

class FirstPage extends StatelessWidget {
  FirstPage({Key? key}) : super(key: key);

  final SnackBar _snackBar = SnackBar(
    content: Row(
      children: [
        Expanded(
          child: Text('Message'),
        ),
      ],
    ),
    duration: const Duration(seconds: 3),
    padding: EdgeInsets.all(15.0),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: const [0.4, 0.6],
            colors: [
              Colors.purple[300]!.withAlpha(100),
              Colors.blue[500]!.withAlpha(100),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          pushPictureView(context);
        },
        child: const Icon(Icons.keyboard_arrow_right),
      ),
    );
  }

  void pushPictureView(BuildContext context) {
    Navigator.of(context).push(
      PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) {
          animation.addStatusListener((status) {
            if (status == AnimationStatus.dismissed) {
              pushPictureView(context);
            } else if (status == AnimationStatus.completed) {
              ScaffoldMessenger.of(context).showSnackBar(_snackBar);
              // ScaffoldMessenger.of(context).clearSnackBars();
            }
          });
          return ListenableProvider(
            create: (context) => animation,
            child: PictureView(
              imageUrl1:
                  'http://www.snut.fr/wp-content/uploads/2015/12/image-de-nature-9.jpg',
              imageUrl2:
                  'https://archzine.fr/wp-content/uploads/2016/01/image-de-neige-fond-d-ecran-montagne-maison-cosy.jpg',
            ),
          );
        },
        transitionDuration: const Duration(milliseconds: 800),
      ),
    );
  }
}
