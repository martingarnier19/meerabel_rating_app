import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class Picture extends StatelessWidget {
  const Picture({
    Key? key,
    required this.imageUrl,
    this.fit,
  }) : super(key: key);

  final String imageUrl;
  final BoxFit? fit;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      placeholder: (context, url) => const Center(
        child: CircularProgressIndicator(),
      ),
      errorWidget: (context, url, error) => const Center(
        child: Icon(
          Icons.error,
        ),
      ),
      fit: fit,
    );
  }
}
