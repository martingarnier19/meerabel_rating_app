import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rating_app/pictures/polaroid_picture.dart';
import 'package:rating_app/pictures/slider_widget.dart';
import 'package:rating_app/pictures/sliding_picture.dart';

enum SelectedPictureEnum {
  bottom,
  top,
  none,
}

const bigPictureHeightRatio = 9 / 10;
const smallPictureHeightRatio = 1 / 5;
const smallPictureLeftOffsetRatio = -1 / 8;
const smallPictureWidthRatio = 2 / 5;
const pictureSpace = 15.0;

const globalPadding = 20.0;

const sliderHeight = 30.0;

const animationDuration = Duration(milliseconds: 400);
const animationCurve = Cubic(0.4, -0.4, 0.1, 1.45);

class PictureView extends StatelessWidget {
  final String imageUrl1;
  final String imageUrl2;

  PictureView({
    Key? key,
    required this.imageUrl1,
    required this.imageUrl2,
  }) : super(key: key);

  final List<String> sliderLegend = [
    "Je ne sais pas",
    "A peine",
    "Faiblement",
    "Un peu",
    "Vaguement",
    "Modérément",
    "Plutôt",
    "Beaucoup",
    "Carrément",
    "Considérablement",
    "Infiniment"
  ];

  final ValueNotifier<SelectedPictureEnum> _selectedPicture =
      ValueNotifier(SelectedPictureEnum.none);

  final Key topKey = GlobalKey();
  final Key bottomKey = GlobalKey();

  final topTransformationController = TransformationController();
  final bottomTransformationController = TransformationController();

  final ValueNotifier<int> _currentSliderValue = ValueNotifier(0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: ValueListenableBuilder(
          valueListenable: _selectedPicture,
          builder: (context, SelectedPictureEnum selectedPicture, child) {
            return Text(
              selectedPicture == SelectedPictureEnum.none
                  ? 'Quelle image préférez-vous ?'
                  : 'Notez cette image',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
              ),
            );
          },
        ),
        leading: ValueListenableBuilder(
          valueListenable: _selectedPicture,
          builder: (context, SelectedPictureEnum selectedPicture, child) {
            if (selectedPicture != SelectedPictureEnum.none) {
              return IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  if (selectedPicture == SelectedPictureEnum.none) {
                    Navigator.of(context).pop();
                  } else {
                    _selectedPicture.value = SelectedPictureEnum.none;
                  }
                },
              );
            }

            return Container();
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(globalPadding),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: const [0.3, 0.7],
            colors: [
              Colors.purple[300]!.withAlpha(100),
              Colors.blue[500]!.withAlpha(100),
            ],
          ),
        ),
        child: LayoutBuilder(
          builder: (context, constraints) {
            final height = constraints.maxHeight;
            final width = constraints.maxWidth;

            return ValueListenableBuilder(
              valueListenable: _selectedPicture,
              builder: (context, selectedPicture, child) {
                final pictures = [
                  SelectedPictureEnum.top,
                  SelectedPictureEnum.bottom,
                ].map((position) {
                  return AnimatedPositioned(
                    key: position == SelectedPictureEnum.bottom
                        ? bottomKey
                        : topKey,
                    duration: animationDuration,
                    curve: animationCurve,
                    top: position == SelectedPictureEnum.top
                        ? selectedPicture == SelectedPictureEnum.bottom
                            ? height * (1 - smallPictureHeightRatio)
                            : selectedPicture == position
                                ? sliderHeight + globalPadding
                                : 0
                        : null,
                    bottom: position == SelectedPictureEnum.bottom
                        ? selectedPicture == SelectedPictureEnum.bottom
                            ? height * (1 - bigPictureHeightRatio) -
                                sliderHeight -
                                globalPadding
                            : 0
                        : null,
                    height: selectedPicture == SelectedPictureEnum.none
                        ? (height - pictureSpace) / 2
                        : selectedPicture == position
                            ? height * bigPictureHeightRatio
                            : height * smallPictureHeightRatio,
                    width: selectedPicture == position ||
                            selectedPicture == SelectedPictureEnum.none
                        ? width
                        : smallPictureWidthRatio * width,
                    left: selectedPicture == position ||
                            selectedPicture == SelectedPictureEnum.none
                        ? 0
                        : width * smallPictureLeftOffsetRatio,
                    child: SlidingPicture(
                      initialOffsetX:
                          (position == SelectedPictureEnum.bottom ? -1 : 1) *
                              (selectedPicture == SelectedPictureEnum.bottom
                                  ? -1
                                  : 1),
                      animationCurve: animationCurve,
                      child: PolaroidPicture(
                        withBigPadding: selectedPicture == position ||
                            selectedPicture == SelectedPictureEnum.none,
                        imageUrl: position == SelectedPictureEnum.bottom
                            ? imageUrl2
                            : imageUrl1,
                        onPressed: () {
                          _selectedPicture.value = selectedPicture == position
                              ? SelectedPictureEnum.none
                              : position;
                        },
                      ),
                    ),
                  );
                }).toList();

                return Stack(
                  children: [
                    Stack(
                      clipBehavior: Clip.none,
                      children: [
                        // Compute pictures order in the stack
                        ...selectedPicture == SelectedPictureEnum.bottom
                            ? pictures.reversed.toList()
                            : pictures,

                        // The slider
                        AnimatedPositioned(
                          width: width,
                          top: 0,
                          right: selectedPicture == SelectedPictureEnum.none
                              ? -width * 2
                              : 0,
                          duration: animationDuration,
                          curve: animationCurve,
                          child: Container(
                            height: sliderHeight,
                            padding:
                                const EdgeInsets.symmetric(horizontal: 20.0),
                            child: SliderWidget(
                              sliderValue: _currentSliderValue,
                            ),
                          ),
                        ),
                      ],
                    ),

                    // Validate button
                    Align(
                      alignment: Alignment.bottomRight,
                      child: ValueListenableBuilder(
                        valueListenable: _currentSliderValue,
                        builder: (context, int sliderValue, child) {
                          final buttonOn =
                              selectedPicture != SelectedPictureEnum.none &&
                                  sliderValue > 0;

                          return AnimatedOpacity(
                            opacity: buttonOn ? 1 : 0,
                            duration: animationDuration,
                            child: InkWell(
                              onTap: buttonOn
                                  ? () {
                                      Navigator.of(context).pop();
                                    }
                                  : null,
                              child: Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.yellowAccent,
                                  boxShadow: [
                                    BoxShadow(
                                      blurRadius: 10,
                                      color: Colors.grey[400]!,
                                    ),
                                  ],
                                ),
                                padding: const EdgeInsets.all(16.0),
                                margin:
                                    const EdgeInsets.only(right: globalPadding),
                                child: const Icon(
                                  Icons.arrow_forward,
                                  size: 32.0,
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    )
                  ],
                );
              },
            );
          },
        ),
      ),
    );
  }

  String computeSliderLabel(int sliderValue) {
    String legend = "";
    if (sliderValue < sliderLegend.length) {
      legend = sliderLegend[sliderValue];
    }

    return "+$sliderValue\n$legend";
  }
}
