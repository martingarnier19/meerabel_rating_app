import 'package:flutter/material.dart';
import 'package:rating_app/pictures/picture.dart';

const smallPadding = 10.0;
const bigPadding = 40.0;
const borderRadius = 5.0;

class PolaroidPicture extends StatelessWidget {
  final String imageUrl;
  final Duration animationDuration;
  final bool withBigPadding;

  final Function()? onPressed;

  const PolaroidPicture({
    Key? key,
    required this.imageUrl,
    this.animationDuration = const Duration(milliseconds: 1),
    this.withBigPadding = true,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: AnimatedContainer(
        duration: animationDuration,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(borderRadius),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              blurRadius: smallPadding,
              color: Colors.grey[400]!,
            ),
          ],
        ),
        padding: const EdgeInsets.fromLTRB(
              smallPadding,
              smallPadding,
              smallPadding,
              bigPadding,
            ) /
            (withBigPadding ? 1 : 2),
        child: Container(
          color: Colors.grey[100],
          child: Picture(
            imageUrl: imageUrl,
            fit: BoxFit.contain,
          ),
        ),
      ),
    );
  }
}
