import 'package:flutter/material.dart';

class SliderWidget extends StatelessWidget {
  final double sliderHeight;
  final int min;
  final int max;

  final ValueNotifier<int> sliderValue;

  SliderWidget({
    Key? key,
    this.sliderHeight = 48,
    this.max = 10,
    this.min = 0,
    sliderValue,
  })  : sliderValue = sliderValue ?? ValueNotifier(0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    double horizontalPadding = sliderHeight * 0.2;

    final texts = [min, max].map(
      (e) => <Widget>[
        Text(
          e.toString(),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: sliderHeight * 1 / 3,
            fontWeight: FontWeight.w700,
            color: Colors.black,
          ),
        ),
        SizedBox(
          width: horizontalPadding * 0.5,
        ),
      ],
    );

    return Container(
      height: sliderHeight,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(sliderHeight * 1 / 3),
        color: Colors.yellow,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: horizontalPadding,
        ),
        child: Row(
          children: <Widget>[
            ...texts.first,
            Expanded(
              child: Center(
                child: SliderTheme(
                  data: SliderTheme.of(context).copyWith(
                    activeTrackColor: Colors.transparent,
                    inactiveTrackColor: Colors.transparent,
                    thumbShape: CustomSliderThumbCircle(
                      thumbRadius: sliderHeight * 0.5,
                      min: min,
                      max: max,
                    ),
                    trackHeight: 5,
                    activeTickMarkColor: Colors.black,
                    inactiveTickMarkColor: Colors.black,
                    tickMarkShape:
                        const RoundSliderTickMarkShape(tickMarkRadius: 2.5),
                  ),
                  child: ValueListenableBuilder(
                    valueListenable: sliderValue,
                    builder: (context, int value, child) {
                      return Slider(
                        value: value.toDouble(),
                        min: min.toDouble(),
                        max: max.toDouble(),
                        divisions: max,
                        onChanged: (newValue) {
                          sliderValue.value = newValue.round();
                        },
                      );
                    },
                  ),
                ),
              ),
            ),
            ...texts.last.reversed,
          ],
        ),
      ),
    );
  }
}

class CustomSliderThumbCircle extends SliderComponentShape {
  final double thumbRadius;
  final int min;
  final int max;

  const CustomSliderThumbCircle({
    required this.thumbRadius,
    this.min = 0,
    this.max = 10,
  });

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(thumbRadius);
  }

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    required Animation<double> activationAnimation,
    required Animation<double> enableAnimation,
    required bool isDiscrete,
    required TextPainter labelPainter,
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required TextDirection textDirection,
    required double value,
    required double textScaleFactor,
    required Size sizeWithOverflow,
  }) {
    final Canvas canvas = context.canvas;
    final paint = Paint()..color = Colors.white;

    TextPainter tp = TextPainter(
      text: TextSpan(
        style: TextStyle(
          fontSize: thumbRadius * 0.8,
          fontWeight: FontWeight.w700,
          color: Colors.black,
        ),
        text: getValue(value),
      ),
      textAlign: TextAlign.center,
      textDirection: TextDirection.ltr,
    );
    tp.layout();
    Offset textCenter = Offset(
      center.dx - (tp.width / 2),
      center.dy - (tp.height / 2),
    );

    canvas.drawCircle(center, thumbRadius * 0.9, paint);
    tp.paint(canvas, textCenter);
  }

  String getValue(double value) {
    return (min + (max - min) * value).round().toString();
  }
}
