import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SlidingPicture extends StatelessWidget {
  final Curve? animationCurve;

  final double initialOffsetX;

  final Widget child;

  const SlidingPicture({
    Key? key,
    required this.initialOffsetX,
    required this.child,
    this.animationCurve,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final animation = Provider.of<Animation<double>>(context, listen: false);

    return AnimatedBuilder(
      animation: animation,
      builder: (context, child) {
        return SlideTransition(
          position: Tween<Offset>(
            begin: Offset(initialOffsetX, 0),
            end: const Offset(0, 0),
          ).animate(
            CurvedAnimation(
              curve: animationCurve ?? Curves.linear,
              parent: animation,
            ),
          ),
          child: child,
        );
      },
      child: child,
    );
  }
}
